#include <mpi.h>
#include <complex>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <getopt.h>

#include "fft.h"

int data_type = 0;
bool is_result_output = false;
bool is_nonzero = false;
const char* output_filename = NULL;
int dimx = 100, dimy = 100, dimz = 100;


void print_usage(help_printer hprt) {
    fprintf(stderr, "FFT benchmark in MPI\n\n"
                    "-x, -y, -z dim    set correspondent dimension to dim\n"
                    "-o filename         specify the output filename\n"
                    "-r                  use non-zero values for benchmark\n");
    if(hprt) {
        hprt();
    }
}

/**
 * Process the command line arguments
 */
void process_opt(int argc, char** argv, int rank, MPI_Comm comm, const char* addon, opt_handler hopt, help_printer hprt) {
    int option = 0;
    char buf[256];
    if(addon) {
        sprintf(buf, "x:y:z:o:rh%s", addon);
    }
    else {
        sprintf(buf, "x:y:z:o:rh");
    }
    while((option = getopt(argc, argv, buf)) != -1) {
        switch(option) {
            case 'x':
                dimx = atoi(optarg);
                break;
            case 'y':
                dimy = atoi(optarg);
                break;
            case 'z':
                dimz = atoi(optarg);
                break;
            case 'o':
                is_result_output = true;
                output_filename = optarg;
                break;
            case 'r':
                data_type = 1;
                break;
            default:
                if(hopt)
                    hopt(option, optarg);
                break;
            case '?':
            case 'h':
                if(rank == 0) {
                    print_usage(hprt);
                }
                MPI_Abort(comm, -1);
        }
    }
    if(is_result_output && output_filename == NULL) {
        output_filename = "output.bin";
    }
}


double get_tick() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (tv.tv_sec + 1.0e-6 * tv.tv_usec);
}

/**
 * Write FFT result to a file
 * The data needs to be transposed correctly to x*y*z
 *
 * comm MPI communicator
 * filename file to write
 * data buffer to the data
 * ax, ay, az dimension for the whole array
 * nx, ny, nz dimension for the local array
 * off_x, off_y offset of local array in the whole array
 */
void writeFFT(MPI_Comm comm, const char* filename, std::complex<double>* data, int ax, int ay, int az, int nx, int ny, int nz, int off_x, int off_y) {
    MPI_Datatype grid;
    int start[4] = {off_x, off_y, 0, 0};
    int arrsize[4] = {ax, ay, az, 2};
    int localsize[4] = {nx, ny, nz, 2};
    MPI_File fh;
    MPI_Comm file_comm;
    MPI_Group world_group, file_group;

    int i, new_size = 0, world, mysize;
    int *sub_size;
    int r;
    MPI_Comm_size(comm, &world);
    MPI_Comm_rank(comm, &r);
    sub_size = (int*)malloc(world * sizeof(int));
    mysize = nx * ny * nz;
    MPI_Allgather(&mysize, 1, MPI_INT, sub_size, 1, MPI_INT, comm);
    for(i = 0; i < world; i++) {
        if(sub_size[i] > 0) {
            sub_size[new_size++] = i;
        }
    }
    MPI_Comm_group(MPI_COMM_WORLD, &world_group);
    MPI_Group_incl(world_group, new_size, sub_size, &file_group);
    MPI_Comm_create(MPI_COMM_WORLD, file_group, &file_comm);
    if(mysize > 0) {
        MPI_File_open(file_comm, const_cast<char*>(filename), MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
        MPI_Type_create_subarray(4, arrsize, localsize, start, MPI_ORDER_C, MPI_DOUBLE, &grid);
        MPI_Type_commit(&grid);

        MPI_File_set_view(fh, 0, MPI_DOUBLE, grid, "native", MPI_INFO_NULL);
        MPI_File_write_all(fh, data, nx * ny * nz * 2, MPI_DOUBLE, MPI_STATUS_IGNORE);
        MPI_File_close(&fh);
        MPI_Comm_free(&file_comm);
    }
    free(sub_size);
    MPI_Group_free(&world_group);
    MPI_Group_free(&file_group);
}

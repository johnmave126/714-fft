#include <stdio.h>
#include <math.h>
#include <string.h>
#include <hclient.h>

#include "fft.h"

#define TEST_LIM 4000

//Imaginary parameters
long IP1, IP2, IP3;
double DP1, DP2, DP3, DP4;

double step(double x, double t) {
    return x>t?1:0;
}

//Imaginary timing function
double time_f() {
    return fabs(sin(DP1 / DP3) * exp(-2*DP2*DP2*DP2+3*DP2*DP2-5*DP2 * DP1+4 - DP4 * DP1) + step(IP1, DP2 * 4) * 4 * IP2 * tan(DP4) + IP2 * IP3);
}


//Imaginary work
int work() {
    int i, j;
    int x, y;
    int ans = 0;
    for(i = 0; i < 2000; i++) {
        for(j = 0; j < 2000; j++) {
            x = i * j;
            y = i / (j+1);
            ans |= x ^ y;
            ans >>= 8;
        }
    }
    return ans;
}

int main(int argc, char** argv) {
    int i;
    double time_start, time_end, time_elapsed;

    int flag;
    int control = 0;

    int total, rank;

    hdesc_t* hdesc;
    hdef_t* hdef;
    htask_t* htask;
    
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &total);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if(rank == 0) {
        hdesc = ah_alloc();
        ah_args(hdesc, &argc, argv);
        ah_connect(hdesc, NULL, 0);
        hdef = ah_def_load("ah_test.def");
        htask = ah_start(hdesc, hdef);
        ah_def_free(hdef);
        if(!htask) {
            htask = ah_join(hdesc, "ah_test.def");
        }
        if(!htask) {
            printf("%s\n", ah_error());
            MPI_Abort(MPI_COMM_WORLD, -1);
        }
        ah_bind_int(htask, "IP1", &IP1);
        ah_bind_int(htask, "IP2", &IP2);
        ah_bind_int(htask, "IP3", &IP3);

        ah_bind_real(htask, "DP1", &DP1);
        ah_bind_real(htask, "DP2", &DP2);
        ah_bind_real(htask, "DP3", &DP3);
        ah_bind_real(htask, "DP4", &DP4);
    }

    flag = 1;
    while(flag && control < TEST_LIM) {
        if(rank == 0) {
            int m = ah_fetch(htask);
        }
        MPI_Barrier(MPI_COMM_WORLD);
        if(rank == 0) {
            time_start = get_tick();
        }
        MPI_Barrier(MPI_COMM_WORLD);
        work();
        MPI_Barrier(MPI_COMM_WORLD);
        if(rank == 0) {
            time_end = get_tick();
            time_elapsed = time_end - time_start + time_f();
            ah_report(htask, &time_elapsed);
            flag = !ah_converged(htask);
        }
        MPI_Bcast(&flag, 1, MPI_INT, 0, MPI_COMM_WORLD);
        control++;
    }
    if(rank == 0) {
        ah_leave(htask);
        ah_free(hdesc);
    }
    MPI_Finalize();
    return 0;
}

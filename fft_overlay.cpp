#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <unistd.h>
#include <complex>
#include <map>
#include <algorithm>
#include <vector>
#include <mpi.h>
#include <fftw3.h>
#include <fftw3-mpi.h>
#include <hclient.h>

#include "fft.h"

#define BIND(var) ah_bind_int(htask, #var, &(parameters.var))
#define OFF(var, i) para_offset[i] = offsetof(tune, var)
#define P(var) (parameters.var)
typedef struct _tune {
    // Auto-tuning parameters
    long D; // # processes on x-dim
    long T1; //# elements on x-dim in a communication tile (Phase1)
    long W1; //max # tiles for concurrent communication (Phase1)
    long Px1; //# elements on x-dim in a sub-tile during Pack1
    long Py1; //# elements on y-dim in a sub-tile during Pack1
    long Fz; //# MPI Test calls during FFTz for one tile
    long Fp1; //# MPI Test calls during Pack1 for one tile
    long Ux1; //# elements on x-dim in a sub-tile during Unpack1
    long Uz1; //# elements on z-dim in a sub-tile during Unpack1
    long Fu1; //# MPI Test calls during Unpack1 for one tile
    long Fy1; //# MPI Test calls during FFTy1 for one tile
    long Ry; //computation ratio of FFTy1 to total FFTy
    long T2; //# elements on z-dim in a communication tile (Phase2)
    long W2; //max # tiles for concurrent communication (Phase2)
    long Pz2; //# elements on z-dim in a sub-tile during Pack2
    long Px2; //# elements on x-dim in a sub-tile during Pack2
    long Fy2; //# MPI Test calls during FFTy2 for one tile
    long Fp2; //# MPI Test calls during Pack2 for one tile
    long Uz2; //# elements on z-dim in a sub-tile during Unpack2
    long Uy2; //# elements on y-dim in a sub-tile during Unpack2
    long Fu2; //# MPI Test calls during Unpack2 for one tile
    long Fx; //# MPI Test calls during FFTx for one tile
    long V; //switch to use MPI Ialltoallv or not
    long S; //switch to use the stride method or not
} tune;
tune parameters;
#define L(var) var = P(var)
#define L2(var) var = 1<<P(var)
#define LOCALIZE() long L2(D), L2(T1), L2(W1), L2(Px1), L2(Py1), L2(Fz), \
                    L2(Fp1), L2(Ux1), L2(Uz1), L2(Fu1), L2(Fy1), L(Ry), \
                    L2(T2), L2(W2), L2(Pz2), L2(Px2), L2(Fy2), L2(Fp2), \
                    L2(Uz2), L2(Uy2), L2(Fu2), L2(Fx), L(V), L(S)

std::complex<double> *data, *xyz_data, *zxy_data, *zyx_data, *pack1_buffer, *unpack1_buffer, *pack2_buffer, *unpack2_buffer;
int total, rank;
ptrdiff_t local_x_start, local_x, local_y_start, local_y, local_y2_start, local_y2, local_z_start, local_z, alloc_local_xyz, alloc_local_zxy, alloc_local_zyx;
ptrdiff_t local_x_V, local_y_V, local_z_V, alloc_local_xyz_V, alloc_local_zxy_V, alloc_local_zyx_V;
ptrdiff_t pack1_uniformsize, pack2_uniformsize;
int ipx, ipy;
int Dy;
bool pack1_V = false, pack2_V = false;

void init_data() {
    int i, j, k;
    LOCALIZE();
    data_f func;
    switch (data_type) {
    case DATA_REAL:
        func = &sine_step_data;
        break;
    case DATA_ZERO:
    default:
        func = &all_zero_data;
        break;
    }
    Dy = total / D;
    ipx = rank / Dy;
    ipy = rank - ipx * Dy;
    local_x_start = ipx * dimx / D;
    local_x = (ipx + 1) * dimx / D - local_x_start;
    local_y_start = ipy * dimy / Dy;
    local_y = (ipy + 1) * dimy / Dy - local_y_start;
    local_y2_start = ipx * dimy / D;
    local_y2 = (ipx + 1) * dimy / D - local_y2_start;
    local_z_start = ipy * dimz / Dy;
    local_z = (ipy + 1) * dimz / Dy - local_z_start;

    alloc_local_xyz = local_x * local_y * dimz;
    alloc_local_zxy = local_z * local_x * dimy;
    alloc_local_zyx = local_z * local_y2 * dimx;
    //data = reinterpret_cast<std::complex<double>*>(fftw_alloc_complex(alloc_local_xyz));
    xyz_data = reinterpret_cast<std::complex<double>*>(fftw_alloc_complex(std::max(alloc_local_xyz, alloc_local_zyx)));
    zxy_data = reinterpret_cast<std::complex<double>*>(fftw_alloc_complex(alloc_local_zxy));
    zyx_data = xyz_data;

    if (V == 0 || V == 1) {
        pack1_V = true;
        local_x_V = dimx - (dimx / D) * (D - 1);
        local_y_V = dimy - (dimy / Dy) * (Dy - 1);
        local_z_V = dimz - (dimz / Dy) * (Dy - 1);
        pack1_uniformsize = T1 * local_y_V * local_z_V;
        alloc_local_xyz_V = ceil(1.0 * local_x_V / T1) * pack1_uniformsize * Dy;
        alloc_local_zxy_V = alloc_local_xyz_V;
        pack1_buffer = reinterpret_cast<std::complex<double>*>(fftw_alloc_complex(alloc_local_xyz_V));
        unpack1_buffer = reinterpret_cast<std::complex<double>*>(fftw_alloc_complex(alloc_local_zxy_V));
    }
    else {
        pack1_V = false;
        pack1_buffer = reinterpret_cast<std::complex<double>*>(fftw_alloc_complex(alloc_local_xyz));
        unpack1_buffer = reinterpret_cast<std::complex<double>*>(fftw_alloc_complex(alloc_local_zxy));
    }

    if (V == 0 || V == 2) {
        pack2_V = true;
        local_x_V = dimx - (dimx / D) * (D - 1);
        local_y_V = dimy - (dimy / D) * (D - 1);
        local_z_V = dimz - (dimz / Dy) * (Dy - 1);
        pack2_uniformsize = T2 * local_x_V * local_y_V;
        alloc_local_zxy_V = ceil(1.0 * local_z_V / T2) * pack2_uniformsize * D;
        alloc_local_zyx_V = alloc_local_zxy_V;
        pack2_buffer = reinterpret_cast<std::complex<double>*>(fftw_alloc_complex(alloc_local_zxy_V));
        unpack2_buffer = reinterpret_cast<std::complex<double>*>(fftw_alloc_complex(alloc_local_zyx_V));
    }
    else  {
        pack2_V = false;
        pack2_buffer = reinterpret_cast<std::complex<double>*>(fftw_alloc_complex(alloc_local_zxy));
        unpack2_buffer = reinterpret_cast<std::complex<double>*>(fftw_alloc_complex(alloc_local_zyx));
    }

    for (i = 0; i < local_x; i++) {
        for (j = 0; j < local_y; j++) {
            for (k = 0; k < dimz; k++) {
                xyz_data[i * local_y * dimz + j * dimz + k] = func(local_x_start + i, local_y_start + j, k);
            }
        }
    }
}

void copy_data() {
    //memcpy(xyz_data, data, alloc_local_xyz * sizeof(std::complex<double>));
}

void free_data() {
    //fftw_free(data);
    fftw_free(xyz_data);
    fftw_free(zxy_data);
    //fftw_free(zyx_data);
    fftw_free(pack1_buffer);
    fftw_free(unpack1_buffer);
    fftw_free(pack2_buffer);
    fftw_free(unpack2_buffer);
}

void do_computation() {
    LOCALIZE(); //Make parameters local vairables
    S = 0; // Disable parameter S for now

    //pre-generate fft 1d plans
    fftw_plan plan_fftx, plan_ffty1, plan_ffty2, plan_fftz;
    plan_fftz = fftw_plan_dft_1d(dimz, reinterpret_cast<double(*)[2]>(xyz_data), reinterpret_cast<double(*)[2]>(xyz_data), FFTW_FORWARD, FFTW_ESTIMATE);
    plan_ffty1 = fftw_plan_dft_1d(dimy, reinterpret_cast<double(*)[2]>(zxy_data), reinterpret_cast<double(*)[2]>(zxy_data), FFTW_FORWARD, FFTW_ESTIMATE);
    plan_ffty2 = fftw_plan_dft_1d(dimy, reinterpret_cast<double(*)[2]>(zxy_data), reinterpret_cast<double(*)[2]>(zxy_data), FFTW_FORWARD, FFTW_ESTIMATE);
    plan_fftx = fftw_plan_dft_1d(dimx, reinterpret_cast<double(*)[2]>(zyx_data), reinterpret_cast<double(*)[2]>(zyx_data), FFTW_FORWARD, FFTW_ESTIMATE);

    //Generate sub communicators
    MPI_Comm phase1_comm, phase2_comm;
    MPI_Comm_split(MPI_COMM_WORLD, ipx, ipy, &phase1_comm);
    MPI_Comm_split(MPI_COMM_WORLD, ipy, ipx, &phase2_comm);

    //Compute phase1 alltoall splitter
    // Dy | dimy => sending same dimension on y
    // Dy | dimz => receiving same dimension alone z
    bool pack1_alltoall = false, pack2_alltoall = false;
    if (dimy % Dy == 0 && dimz % Dy == 0) {
        pack1_alltoall = true;
    }
    if (dimx % D == 0 && dimy % D == 0) {
        pack2_alltoall = true;
    }

    //Need to use different sending and receiving sizes if above condition isn't met
    int *pack1_sendsizes, *pack1_recvsizes;
    int *pack1_sendstart, *pack1_recvstart;
    if (!pack1_alltoall && !pack1_V) {
        pack1_sendsizes = (int*)malloc(Dy * sizeof(int));
        pack1_recvsizes = (int*)malloc(Dy * sizeof(int));
        pack1_sendstart = (int*)malloc(Dy * sizeof(int));
        pack1_recvstart = (int*)malloc(Dy * sizeof(int));

        for (int i = 0; i < Dy; i++) {//For each process in the tile
            int target_y_start = i * dimy / Dy;
            int target_y_size = (i + 1) * dimy / Dy - target_y_start;
            int target_z_start = i * dimz / Dy;
            int target_z_size = (i + 1) * dimz / Dy - target_z_start;
            pack1_sendsizes[i] = 2 * T1 * local_y * target_z_size;
            pack1_sendstart[i] = 2 * T1 * local_y * target_z_start;
            pack1_recvsizes[i] = 2 * T1 * target_y_size * local_z;
            pack1_recvstart[i] = 2 * T1 * target_y_start * local_z;
        }
    }
    int *pack2_sendsizes, *pack2_recvsizes;
    int *pack2_sendstart, *pack2_recvstart;
    if (!pack2_alltoall && !pack2_V) {
        pack2_sendsizes = (int*)malloc(D * sizeof(int));
        pack2_recvsizes = (int*)malloc(D * sizeof(int));
        pack2_sendstart = (int*)malloc(D * sizeof(int));
        pack2_recvstart = (int*)malloc(D * sizeof(int));

        for (int i = 0; i < D; i++) {//For each process in the tile
            int target_x_start = i * dimx / D;
            int target_x_size = (i + 1) * dimx / D - target_x_start;
            int target_y_start = i * dimy / D;
            int target_y_size = (i + 1) * dimy / D - target_y_start;
            pack2_sendsizes[i] = 2 * T2 * local_x * target_y_size;
            pack2_sendstart[i] = 2 * T2 * local_x * target_y_start;
            pack2_recvsizes[i] = 2 * T2 * target_x_size * local_y2;
            pack2_recvstart[i] = 2 * T2 * target_x_start * local_y2;
        }
    }

    int fftz_testc = 0, p1_testc = 0, ffty1_testc = 0, u1_testc = 0, ffty2_testc = 0, p2_testc = 0, fftx_testc = 0, u2_testc = 0;

    // divide data into k1 tiles on the x dimension
    int k1 = ceil(1.0 * local_x / T1);
    MPI_Request *phase1_requests = (MPI_Request*)malloc(k1 * sizeof(MPI_Request));
    for (int i = 0; i < k1 + W1; i++) { // this for loop is over processors
        int tile_start;
        int tile_size; //Tile starting point and size on dim x
        if (i < k1) {  // fftz & pack1 on tile i
            tile_start = i * T1;
            tile_size = std::min(local_x, (i + 1) * T1) - tile_start;
            int tile_sendbuffer_start;
            if (pack1_V) {
                tile_sendbuffer_start = i * pack1_uniformsize * Dy;
            }
            else {
                tile_sendbuffer_start = tile_start * local_y * dimz; //Since sending buffer cannot be reused, shift the current tile in the buffer
            }
            //Advance pointer by tile size
            for (int bx = 0; bx < tile_size; bx += Px1) {
                for (int by = 0; by < local_y; by += Py1) {
                    //Looptiling
                    int subtile_x_size = std::min(Px1, (long)tile_size - bx);
                    int subtile_y_size = std::min(Py1, (long)local_y - by);
                    for (int sx = 0; sx < subtile_x_size; sx++) {
                        for (int sy = 0; sy < subtile_y_size; sy++) {//For each stencil in the subtile
                            //Current stencil is  (bx + sx, by + sy, 0<->dimz)
                            fftw_execute_dft(
                                plan_fftz,
                                reinterpret_cast<double(*)[2]>(xyz_data + (((tile_start + bx + sx) * local_y) + by + sy) * dimz),
                                reinterpret_cast<double(*)[2]>(xyz_data + (((tile_start + bx + sx) * local_y) + by + sy) * dimz)
                            ); // Perfrom FFTz using precomputed plan
                            //For frequency Fz need to call MPI_Test to progress the communication
                            fftz_testc++;
                            if (fftz_testc % Fz == 0) {
                                fftz_testc = 0;
                                int dummy;
                                MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, phase1_comm, &dummy, MPI_STATUSES_IGNORE);
                            }
                        }
                    }
                    //Pack1
                    for (int sx = 0; sx < subtile_x_size; sx++) {
                        for (int sy = 0; sy < subtile_y_size; sy++) {//For each stencil in the subtile
                            //Need to go alone z dimension of xyz_data for spatial locality
                            //But the stencil need to be broken in to Dy pieces putting to different locations of buffer
                            for (int pz = 0; pz < Dy; pz++) {
                                //pack1_buffer layout will be
                                // Block_1 Block_2 Block_3 ... Block_{Dy}
                                // for each block, the data is in x-z-y layout
                                int target_subtile_z_start = pz * dimz / Dy; //start z coord of current block
                                int target_subtile_z_end = (pz + 1) * dimz / Dy;
                                int target_subtile_z_size = target_subtile_z_end - target_subtile_z_start; //size of the current block
                                int target_base;
                                if (pack1_V) {
                                    target_base = pz * pack1_uniformsize;
                                }
                                else {
                                    target_base = tile_size * local_y * target_subtile_z_start;//start address of current block
                                }
                                for (int sz = target_subtile_z_start; sz < target_subtile_z_end; sz++) {
                                    //Looping through z-axis of pz-th process
                                    //Current xyz_data coordinate is (bx + sx, by + sy, sz)
                                    //Globally the coordinate is (bx + sx, local_y_start + by + sy, sz)
                                    //Target coordinate is also (bx + sx, by + sy, sz) but with last 2 axis-s inverted
                                    pack1_buffer[tile_sendbuffer_start + target_base + ((bx + sx) * target_subtile_z_size + sz - target_subtile_z_start) * local_y + by + sy] = xyz_data[((tile_start + bx + sx) * local_y + by + sy) * dimz + sz];
                                }
                                //For frequency Fp1 need to call MPI_Test to progress the communication
                                p1_testc++;
                                if (p1_testc % Fp1 == 0) {
                                    p1_testc = 0;
                                    int dummy;
                                    MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, phase1_comm, &dummy, MPI_STATUSES_IGNORE);
                                }
                            }
                        }
                    }
                }
            }
        }

        if (i >= W1) { // wait on tile i-W1
            MPI_Wait(&phase1_requests[i - W1], MPI_STATUS_IGNORE);
        }

        if (i < k1) { // non-blocking alltoall on tile i
            //if tile_size != T1, a smaller buffer is sent
            int tile_sendbuffer_start;
            int tile_recvbuffer_start;
            if (pack1_V) {
                tile_sendbuffer_start = i * pack1_uniformsize * Dy;
                tile_recvbuffer_start = i * pack1_uniformsize * Dy;
            }
            else {
                tile_sendbuffer_start = tile_start * local_y * dimz; //Since sending buffer cannot be reused, shift the current tile in the buffer
                tile_recvbuffer_start = tile_start * local_z * dimy; //Same with receiving buffer
            }
            if (tile_size != T1 && !pack1_alltoall && !pack1_V) {
                //Reconstruct sizes
                for (int j = 0; j < Dy; j++) {
                    int target_y_start = j * dimy / Dy;
                    int target_y_size = (j + 1) * dimy / Dy - target_y_start;
                    int target_z_start = j * dimz / Dy;
                    int target_z_size = (j + 1) * dimz / Dy - target_z_start;
                    pack1_sendsizes[j] = 2 * tile_size * local_y * target_z_size;
                    pack1_recvsizes[j] = 2 * tile_size * target_y_size * local_z;
                    pack1_sendstart[j] = 2 * tile_size * local_y * target_z_start;
                    pack1_recvstart[j] = 2 * tile_size * target_y_start * local_z;
                }
            }
            if (pack1_alltoall) {//All processes sending and receiving same size
                MPI_Ialltoall(pack1_buffer + tile_sendbuffer_start, 2 * tile_size * local_y * local_z, MPI_DOUBLE,
                              unpack1_buffer + tile_recvbuffer_start, 2 * tile_size * local_y * local_z, MPI_DOUBLE,
                              phase1_comm, &phase1_requests[i]);
            }
            else {
                if (!pack1_V) {
                    MPI_Ialltoallv(pack1_buffer + tile_sendbuffer_start, pack1_sendsizes, pack1_sendstart, MPI_DOUBLE,
                                   unpack1_buffer + tile_recvbuffer_start, pack1_recvsizes, pack1_recvstart, MPI_DOUBLE,
                                   phase1_comm, &phase1_requests[i]);
                }
                else {
                    MPI_Ialltoall(pack1_buffer + tile_sendbuffer_start, 2 * pack1_uniformsize, MPI_DOUBLE,
                                  unpack1_buffer + tile_recvbuffer_start, 2 * pack1_uniformsize, MPI_DOUBLE,
                                  phase1_comm, &phase1_requests[i]);
                }
            }
        }

        if (i >= W1) {
            tile_start = (i - W1) * T1;
            tile_size = std::min(local_x, (i + 1 - W1) * T1) - tile_start;
            int tile_recvbuffer_start;
            if (pack1_V) {
                tile_recvbuffer_start = (i - W1) * pack1_uniformsize * Dy;
            }
            else {
                tile_recvbuffer_start = tile_start * local_z * dimy;
            }
            // Unpack1 and FFTy1 on tile (i-W1)
            // Unpack1: unpack1_buffer in block layout
            // Block_1 Block_2 Block_3 ... Block_{Dy}
            // Each block in x-z-y form
            // Need to convert to global x-z-y form
            // For each y-stencil, copy it to appropriate location in the zxy_data

            //Looptiling on x-z plane
            for (int bx = 0; bx < tile_size; bx += Ux1) {
                for (int bz = 0; bz < local_z; bz += Uz1) {
                    //Looptiling
                    int subtile_x_size = std::min(Ux1, (long)tile_size - bx);
                    int subtile_z_size = std::min(Uz1, (long)local_z - bz);
                    for (int sx = 0; sx < subtile_x_size; sx++) {
                        for (int sz = 0; sz < subtile_z_size; sz++) {//For each stencil in the subtile
                            //Current stencil is  (tile_start + bx + sx, 0<->dimz, bz + sz)
                            //Unpack from unpack1_buffer
                            //TODO: Consider V
                            for (int py = 0; py < Dy; py++) {
                                //The stencil is in Dy pieces scatterred in different blocks
                                int source_subtile_y_start = py * dimy / Dy; //start y coord of current block
                                int source_subtile_y_end = (py + 1) * dimy / Dy;
                                int source_subtile_y_size = source_subtile_y_end - source_subtile_y_start; //size of the current block

                                int source_base;
                                if (pack1_V) {
                                    source_base = py * pack1_uniformsize;
                                }
                                else {
                                    source_base = tile_size * local_z * source_subtile_y_start;//start address of current block
                                }
                                //The substencil is (bx + sx, target_subtile_y_start <-> target_subtile_y_end, bz + sz)
                                //Copy to correspondent xzy_data in x-z-y layout
                                memcpy(
                                    zxy_data + ((tile_start + bx + sx) * local_z + bz + sz) * dimy + source_subtile_y_start,
                                    unpack1_buffer + tile_recvbuffer_start + source_base + (bx + sx) * local_z + bz + sz,
                                    sizeof(std::complex<double>) * source_subtile_y_size
                                );
                                u1_testc++;
                                if (u1_testc % Fu1 == 0) {
                                    u1_testc = 0;
                                    int dummy;
                                    MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, phase1_comm, &dummy, MPI_STATUSES_IGNORE);
                                }
                            }
                        }
                    }
                    //Perfrom FFTy1 on each stencil
                    for (int sx = 0; sx < subtile_x_size; sx++) {
                        for (int sz = 0; sz < subtile_z_size; sz++) {
                            //For each stencil in the subtile
                            //Now apply Ry
                            //Calculate coordinate on x-z plane
                            int global_coord_xz = (local_x_start + tile_start + bx + sx) * dimz + local_z_start + bz + sz;
                            if (global_coord_xz % (11 - Ry) == 0) {
                                //Compute fft only every (11-Ry) elements
                                fftw_execute_dft(
                                    plan_ffty1,
                                    reinterpret_cast<double(*)[2]>(zxy_data + ((tile_start + bx + sx) * local_z + bz + sz) * dimy),
                                    reinterpret_cast<double(*)[2]>(zxy_data + ((tile_start + bx + sx) * local_z + bz + sz) * dimy)
                                ); //Perfrom FFTz using precomputed plan
                            }
                            //For frequency Fy1 need to call MPI_Test to progress the communication
                            ffty1_testc++;
                            if (ffty1_testc % Fy1 == 0) {
                                ffty1_testc = 0;
                                int dummy;
                                MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, phase1_comm, &dummy, MPI_STATUSES_IGNORE);
                            }
                        }
                    }
                }
            }
        }
    }
    free(phase1_requests);

    if (S == 0) {
        // Transpose: Change the memory layout x-z-y to z-x-y
        // Use FFTW for efficiency
        // TO DO: if dimx == dimy no need to transpose
        fftw_plan plan_transpose;
        fftw_iodim transpose_dims[3];
        //Transpose x-z-y to z-x-y
        transpose_dims[0].n = local_x; //x
        transpose_dims[0].is = 2 * local_z * dimy;
        transpose_dims[0].os = 2 * dimy;
        transpose_dims[1].n = local_z; //z
        transpose_dims[1].is = 2 * dimy;
        transpose_dims[1].os = 2 * local_x * dimy;
        transpose_dims[2].n = 2 * dimy; //y
        transpose_dims[2].is = 1;
        transpose_dims[2].os = 1;
        plan_transpose = fftw_plan_guru_r2r(0, NULL, 3, transpose_dims,
                                            reinterpret_cast<double*>(zxy_data), reinterpret_cast<double*>(zxy_data), NULL, FFTW_ESTIMATE);
        fftw_execute(plan_transpose);
        fftw_destroy_plan(plan_transpose);
    }

    // Phase 2

    // divide data into k2 tiles on the z dimension
    int k2 = ceil(1.0 * local_z / T2);
    MPI_Request *phase2_requests = (MPI_Request*)malloc(k2 * sizeof(MPI_Request));
    for (int i = 0; i < k2 + W2; i++) { // this for loop is over processors
        int tile_start;
        int tile_size; //Tile starting point and size on dim x
        if (i < k2) {  // fftz & pack1 on tile i
            tile_start = i * T2;
            tile_size = std::min(local_z, (i + 1) * T2) - tile_start;
            int tile_sendbuffer_start;
            if (pack2_V) {
                tile_sendbuffer_start = i * pack2_uniformsize * D;
            }
            else {
                tile_sendbuffer_start = tile_start * dimy * local_x; //Since sending buffer cannot be reused, shift the current tile in the buffer
            }
            //Advance pointer by tile size
            for (int bz = 0; bz < tile_size; bz += Pz2) {
                for (int bx = 0; bx < local_x; bx += Px2) {
                    //Looptiling
                    int subtile_z_size = std::min(Pz2, (long)tile_size - bz);
                    int subtile_x_size = std::min(Px2, (long)local_x - bx);
                    for (int sz = 0; sz < subtile_z_size; sz++) {
                        for (int sx = 0; sx < subtile_x_size; sx++) {//For each stencil in the subtile

                            int global_coord_xz = (local_x_start + bx + sx) * dimz + local_z_start + tile_start + bz + sz;
                            if (global_coord_xz % (11 - Ry) != 0) {
                                //Compute fft only every (11-Ry) elements

                                //Current stencil is  (bz + sz, bx + sx, 0<->dimy)
                                fftw_execute_dft(
                                    plan_ffty2,
                                    reinterpret_cast<double(*)[2]>(zxy_data + (((tile_start + bz + sz) * local_x) + bx + sx) * dimy),
                                    reinterpret_cast<double(*)[2]>(zxy_data + (((tile_start + bz + sz) * local_x) + bx + sx) * dimy)
                                ); //Perfrom FFTy2 using precomputed plan
                            }
                            //For frequency Fy2 need to call MPI_Test to progress the communication
                            ffty2_testc++;
                            if (ffty2_testc % Fy2 == 0) {
                                ffty2_testc = 0;
                                int dummy;
                                MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, phase2_comm, &dummy, MPI_STATUSES_IGNORE);
                            }
                        }
                    }
                    //Pack2
                    for (int sz = 0; sz < subtile_z_size; sz++) {
                        for (int sx = 0; sx < subtile_x_size; sx++) {//For each stencil in the subtile
                            //Need to go alone y dimension of zxy_data for spatial locality
                            //But the stencil need to be broken in to Dy pieces putting to different locations of buffer
                            for (int py = 0; py < D; py++) {
                                //pack1_buffer layout will be
                                // Block_1 Block_2 Block_3 ... Block_D
                                // for each block, the data is in z-y-x layout
                                int target_subtile_y_start = py * dimy / D; //start z coord of current block
                                int target_subtile_y_end = (py + 1) * dimy / D;
                                int target_subtile_y_size = target_subtile_y_end - target_subtile_y_start; //size of the current block

                                int target_base;
                                if (pack2_V) {
                                    target_base = py * pack2_uniformsize;
                                }
                                else {
                                    target_base = tile_size * local_x * target_subtile_y_start;//start address of current block
                                }
                                for (int sy = target_subtile_y_start; sy < target_subtile_y_end; sy++) {
                                    //Looping through z-axis of pz-th process
                                    //Current zxy_data coordinate is (bz + sz, bx + sx, sy)
                                    //Globally the coordinate is (bz + sz, local_x_start + bx + sx, sy)
                                    //Target coordinate is also (bz + sz, bx + sx, sy) but with last 2 axis-s inverted
                                    pack2_buffer[tile_sendbuffer_start + target_base + ((bz + sz) * target_subtile_y_size + sy - target_subtile_y_start) * local_x + bx + sx] = zxy_data[((tile_start + bz + sz) * local_x + bx + sx) * dimy + sy];
                                }
                                //For frequency Fp2 need to call MPI_Test to progress the communication
                                p2_testc++;
                                if (p2_testc % Fp2 == 0) {
                                    p2_testc = 0;
                                    int dummy;
                                    MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, phase2_comm, &dummy, MPI_STATUSES_IGNORE);
                                }
                            }
                        }
                    }
                }
            }
        }

        if (i >= W2) { // wait on tile i-W2
            MPI_Wait(&phase2_requests[i - W2], MPI_STATUS_IGNORE);
        }

        if (i < k2) { // non-blocking alltoall on tile i
            //if tile_size != T2, a smaller buffer is sent
            //TODO: consider parameter V
            int tile_sendbuffer_start;
            int tile_recvbuffer_start;
            if (pack2_V) {
                tile_sendbuffer_start = i * pack2_uniformsize * D;
                tile_recvbuffer_start = i * pack2_uniformsize * D;
            }
            else {
                tile_sendbuffer_start = tile_start * local_x * dimy; //Since sending buffer cannot be reused, shift the current tile in the buffer
                tile_recvbuffer_start = tile_start * local_y2 * dimx; //Same with receiving buffer
            }
            if (tile_size != T2 && !pack2_alltoall && !pack2_V) {
                //Reconstruct sizes
                for (int j = 0; j < D; j++) {
                    int target_x_start = j * dimx / D;
                    int target_x_size = (j + 1) * dimx / D - target_x_start;
                    int target_y_start = j * dimy / D;
                    int target_y_size = (j + 1) * dimy / D - target_y_start;
                    pack2_sendsizes[j] = 2 * tile_size * local_x * target_y_size;
                    pack2_recvsizes[j] = 2 * tile_size * target_x_size * local_y2;
                    pack2_sendstart[j] = 2 * tile_size * local_x * target_y_start;
                    pack2_recvstart[j] = 2 * tile_size * target_x_start * local_y2;
                }
            }
            if (pack2_alltoall) {//All processes sending and receiving same size
                MPI_Ialltoall(pack2_buffer + tile_sendbuffer_start, 2 * tile_size * local_x * local_y2, MPI_DOUBLE,
                              unpack2_buffer + tile_recvbuffer_start, 2 * tile_size * local_x * local_y2, MPI_DOUBLE,
                              phase2_comm, &phase2_requests[i]);
            }
            else {
                if (!pack2_V) {
                    MPI_Ialltoallv(pack2_buffer + tile_sendbuffer_start, pack2_sendsizes, pack2_sendstart, MPI_DOUBLE,
                                   unpack2_buffer + tile_recvbuffer_start, pack2_recvsizes, pack2_recvstart, MPI_DOUBLE,
                                   phase2_comm, &phase2_requests[i]);
                }
                else {
                    MPI_Ialltoall(pack2_buffer + tile_sendbuffer_start, 2 * pack2_uniformsize, MPI_DOUBLE,
                                  unpack2_buffer + tile_recvbuffer_start, 2 * pack2_uniformsize, MPI_DOUBLE,
                                  phase2_comm, &phase2_requests[i]);
                }
            }
        }

        if (i >= W2) {
            tile_start = (i - W2) * T2;
            tile_size = std::min(local_z, (i + 1 - W2) * T2) - tile_start;
            int tile_recvbuffer_start;
            if (pack2_V) {
                tile_recvbuffer_start = (i - W2) * pack2_uniformsize * D;
            }
            else {
                tile_recvbuffer_start = tile_start * local_y2 * dimx;
            }
            // Unpack2 and FFTy2 on tile (i-W2)
            // Unpack2: unpack2_buffer in block layout
            // Block_1 Block_2 Block_3 ... Block_{Dy}
            // Each block in z-y-x form
            // Need to convert to global z-y-x form
            // For each z-stencil, copy it to appropriate location in the zyx_data

            //Looptiling on y-z plane
            for (int bz = 0; bz < tile_size; bz += Uz2) {
                for (int by = 0; by < local_y2; by += Uy2) {
                    //Looptiling
                    int subtile_z_size = std::min(Uz2, (long)tile_size - bz);
                    int subtile_y_size = std::min(Uy2, (long)local_y2 - by);
                    for (int sz = 0; sz < subtile_z_size; sz++) {
                        for (int sy = 0; sy < subtile_y_size; sy++) {//For each stencil in the subtile
                            //Current stencil is  (tile_start + bx + sx, 0<->dimz, bz + sz)
                            //Unpack from unpack2_buffer
                            //TODO: Consider V
                            for (int px = 0; px < D; px++) {
                                //The stencil is in D pieces scatterred in different blocks
                                int source_subtile_x_start = px * dimx / D; //start y coord of current block
                                int source_subtile_x_end = (px + 1) * dimx / D;
                                int source_subtile_x_size = source_subtile_x_end - source_subtile_x_start; //size of the current block

                                int source_base;
                                if (pack2_V) {
                                    source_base = px * pack2_uniformsize;
                                }
                                else {
                                    source_base = tile_size * local_y2 * source_subtile_x_start;//start address of current block
                                }
                                //The substencil is (bz + sz, target_subtile_x_start <-> target_subtile_x_end, by + sy)
                                //Copy to correspondent zyx_data in z-y-x layout
                                memcpy(
                                    zyx_data + ((tile_start + bz + sz) * local_y2 + by + sy) * dimx + source_subtile_x_start,
                                    unpack2_buffer + tile_recvbuffer_start + source_base + (bz + sz) * local_y2 + by + sy,
                                    sizeof(std::complex<double>) * source_subtile_x_size
                                );
                                //For frequency Fu2 need to call MPI_Test to progress the communication
                                u2_testc++;
                                if (u2_testc % Fu1 == 0) {
                                    u2_testc = 0;
                                    int dummy;
                                    MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, phase2_comm, &dummy, MPI_STATUSES_IGNORE);
                                }
                            }
                        }
                    }
                    //Perfrom FFTx on each stencil
                    for (int sz = 0; sz < subtile_z_size; sz++) {
                        for (int sy = 0; sy < subtile_y_size; sy++) {
                            //For each stencil in the subtile
                            fftw_execute_dft(
                                plan_fftx,
                                reinterpret_cast<double(*)[2]>(zyx_data + ((tile_start + bz + sz) * local_y2 + by + sy) * dimx),
                                reinterpret_cast<double(*)[2]>(zyx_data + ((tile_start + bz + sz) * local_y2 + by + sy) * dimx)
                            ); //Perfrom FFTx using precomputed plan
                            //For frequency Fx need to call MPI_Test to progress the communication
                            fftx_testc++;
                            if (fftx_testc % Fx == 0) {
                                fftx_testc = 0;
                                int dummy;
                                MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, phase2_comm, &dummy, MPI_STATUSES_IGNORE);
                            }
                        }
                    }
                }
            }
        }
    }
    free(phase2_requests);


    if (!pack1_alltoall && !pack1_V) {
        free(pack1_sendsizes);
        free(pack1_recvsizes);
        free(pack1_sendstart);
        free(pack1_recvstart);
    }

    if (!pack2_alltoall && !pack2_V) {
        free(pack2_sendsizes);
        free(pack2_recvsizes);
        free(pack2_sendstart);
        free(pack2_recvstart);
    }


    fftw_destroy_plan(plan_fftz);
    fftw_destroy_plan(plan_ffty1);
    fftw_destroy_plan(plan_ffty2);
    fftw_destroy_plan(plan_fftx);

    MPI_Comm_free(&phase1_comm);
    MPI_Comm_free(&phase2_comm);
}

struct TuneCompare {
    bool operator()(const tune& a, const tune& b) const {
        char *aa = (char*)&a, *bb = (char*)&b;
        for (int i = 0; i < sizeof(tune); i++) {
            if (aa[i] < bb[i]) {
                return true;
            }
            else if (aa[i] > bb[i]) {
                return false;
            }
        }
        return false;
    }
};

std::map<tune, double, TuneCompare> parameter_store;

double global_min;
double start_clock;

void print_parameter(tune &data) {
    long* paras = (long*)(&data);
    for (int i = 0; i < sizeof(tune) / sizeof(long); i++) {
        printf("\t%ld", paras[i]);
    }
    printf("\n");
}

void print_datapoint(tune &data, bool unique, double elap_time) {
    printf("%c\t%.5lf\t%.5lf\t%.5lf", unique ? 'U' : 'D', get_tick() - start_clock, global_min, elap_time);
    print_parameter(data);
}

char logfilename[255] = "ah_log.bin";

void store_parameter(double elap_time) {
    parameter_store[parameters] = elap_time;
    FILE* fp = fopen(logfilename, "a+");
    fwrite(&parameters, sizeof(tune), 1, fp);
    fwrite(&elap_time, sizeof(double), 1, fp);
    fclose(fp);
}

bool check_parameter(htask_t* htask) {
    LOCALIZE();
    if (parameter_store.count(parameters) != 0) {
        double time_elapsed = parameter_store[parameters];
        print_datapoint(parameters, false, time_elapsed);
        ah_report(htask, &time_elapsed);
        return false;
    }
    return true;
}

bool tune_equal(const tune& a, const tune& b) {
    char *aa = (char*)&a, *bb = (char*)&b;
    for (int i = 0; i < sizeof(tune); i++) {
        if (aa[i] != bb[i]) {
            return false;
        }
    }
    return true;
}


int replay_parameter(htask_t* htask) {
    double elap_time;
    tune logged_parameters;
    size_t read_cnt;
    size_t file_loc = 0;
    printf("==Replay Start==\n");
    FILE* fp = fopen(logfilename, "rb");
    if (!fp) {
        return 1;
    }
    while (1) {
        if (feof(fp)) {
            fclose(fp);
            break;
        }
        ah_fetch(htask);
        while (parameter_store.count(parameters) > 0) {
            elap_time = parameter_store[parameters];
            print_datapoint(parameters, false, elap_time);
            ah_report(htask, &elap_time);
            ah_fetch(htask);
        }
        read_cnt = fread(&logged_parameters, sizeof(tune), 1, fp);
        if (read_cnt == 0 && feof(fp)) {
            fclose(fp);
            int dummy = truncate(logfilename, file_loc);
            break;
        }
        read_cnt = fread(&elap_time, sizeof(double), 1, fp);
        if (read_cnt == 0 && feof(fp)) {
            fclose(fp);
            int dummy = truncate(logfilename, file_loc);
            break;
        }
        if (!tune_equal(logged_parameters, parameters)) {
            fclose(fp);
            printf("Parameter Disagree\n");
            printf("Documented: ");
            print_parameter(logged_parameters);
            printf("Given: ");
            print_parameter(parameters);
            int dummy = truncate(logfilename, file_loc);
            break;
        }
        parameter_store[parameters] = elap_time;
        global_min = std::min(global_min, elap_time);
        print_datapoint(parameters, true, elap_time);
        ah_report(htask, &elap_time);
        file_loc += sizeof(tune) + sizeof(double);
    }
    return !ah_converged(htask);
}

void private_help() {
    fprintf(stderr, "-l logfile          use logfile as binary log\n"
            "-s seed             use seed as random seed\n"
            "-p                  use random model for rejection\n");
}

int seed_ah = 42;
bool reject_random = false;

void private_opt(int option, const char* optarg) {
    switch (option) {
    case 'l':
        strcpy(logfilename, optarg);
        break;
    case 's':
        seed_ah = atoi(optarg);
        break;
    case 'p':
        reject_random = true;
    }
}


int countTrailZero(unsigned x) {
    if (x == 0) return 0;
    return log2 (x & -x);
}


void decorate_hdesc(hdef_t* hdef) {
    char buf[1024];
    sprintf(buf, "%d", seed_ah);
    ah_def_cfg(hdef, "RANDOM_SEED", buf);
    sprintf(buf, "D<=%d&&Px1<=T1&&Ux1<=T1&&Pz2<=T2&&Uz2<=T2", countTrailZero(total));
    ah_def_cfg(hdef, "OC_CONSTRAINTS", buf);
    sprintf(buf, "%d", dimx);
    ah_def_cfg(hdef, "VAL_DIMX", buf);
    sprintf(buf, "%d", dimy);
    ah_def_cfg(hdef, "VAL_DIMY", buf);
    sprintf(buf, "%d", dimy);
    ah_def_cfg(hdef, "VAL_DIMY", buf);
    sprintf(buf, "%d", total);
    ah_def_cfg(hdef, "VAL_NPROC", buf);
    if (reject_random) {
        ah_def_cfg(hdef, "REJECT_METHOD", "random");
    }

}

int main(int argc, char **argv) {
    int i;
    double time_start, time_end, time_elapsed;

    int flag, skip_flag;

    hdesc_t* hdesc;
    hdef_t* hdef;
    htask_t* htask;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &total);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    process_opt(argc, argv, rank, MPI_COMM_WORLD, "l:s:p", &private_opt, &private_help);

    if (rank == 0) {
        hdesc = ah_alloc();
        ah_args(hdesc, &argc, argv);
        ah_connect(hdesc, NULL, 0);
        hdef = ah_def_load("./fft_overlay.def");
        decorate_hdesc(hdef);
        htask = ah_start(hdesc, hdef);
        ah_def_free(hdef);
        if (!htask) {
            printf("%s\n", ah_error());
            htask = ah_join(hdesc, "./fft_overlay.def");
        }
        if (!htask) {
            printf("%s\n", ah_error());
            MPI_Abort(MPI_COMM_WORLD, -1);
        }
        BIND(D);
        BIND(T1);
        BIND(W1);
        BIND(Px1);
        BIND(Py1);
        BIND(Fz);
        BIND(Fp1);
        BIND(Ux1);
        BIND(Uz1);
        BIND(Fu1);
        BIND(Fy1);
        BIND(Ry);
        BIND(T2);
        BIND(W2);
        BIND(Pz2);
        BIND(Px2);
        BIND(Fy2);
        BIND(Fp2);
        BIND(Uz2);
        BIND(Uy2);
        BIND(Fu2);
        BIND(Fx);
        BIND(V);
        BIND(S);

        global_min = 1e10;
        start_clock = get_tick();
        printf("==FFT NEW Benchmark==\n");
        printf("==Seed=%d num_proc=%d dimx=%d dimy=%d dimz=%d==\n", seed_ah, total, dimx, dimy, dimz);
    }

    flag = 1;
    MPI_Datatype para_struct;
    int para_blocklen[24];
    MPI_Aint para_offset[24];
    MPI_Datatype para_type[24];
    for (i = 0; i < 24; i++) {
        para_blocklen[i] = 1;
        para_type[i] = MPI_LONG;
    }
    OFF(D, 0);
    OFF(T1, 1);
    OFF(W1, 2);
    OFF(Px1, 3);
    OFF(Py1, 4);
    OFF(Fz, 5);
    OFF(Fp1, 6);
    OFF(Ux1, 7);
    OFF(Uz1, 8);
    OFF(Fu1, 9);
    OFF(Fy1, 10);
    OFF(Ry, 11);
    OFF(T2, 12);
    OFF(W2, 13);
    OFF(Pz2, 14);
    OFF(Px2, 15);
    OFF(Fy2, 16);
    OFF(Fp2, 17);
    OFF(Uz2, 18);
    OFF(Uy2, 19);
    OFF(Fu2, 20);
    OFF(Fx, 21);
    OFF(V, 22);
    OFF(S, 23);
    MPI_Type_create_struct(24, para_blocklen, para_offset, para_type, &para_struct);
    MPI_Type_commit(&para_struct);

    if (rank == 0) {
        flag = replay_parameter(htask);
        printf("==Replay End==\n");
    }
    MPI_Bcast(&skip_flag, 1, MPI_INT, 0, MPI_COMM_WORLD);

    while (flag) {
        skip_flag = false;
        if (rank == 0) {
            ah_fetch(htask);
            if (!check_parameter(htask)) {
                skip_flag = true;
                flag = !ah_converged(htask);
            }
        }
        MPI_Bcast(&skip_flag, 1, MPI_INT, 0, MPI_COMM_WORLD);
        if (skip_flag) {
            MPI_Bcast(&flag, 1, MPI_INT, 0, MPI_COMM_WORLD);
            continue;
        }
        MPI_Bcast(&parameters, 1, para_struct, 0, MPI_COMM_WORLD);
        init_data();
        copy_data();
        MPI_Barrier(MPI_COMM_WORLD);
        if (rank == 0) {
            time_start = get_tick();
        }
        MPI_Barrier(MPI_COMM_WORLD);
        do_computation();
        MPI_Barrier(MPI_COMM_WORLD);
        if (rank == 0) {
            time_end = get_tick();
            time_elapsed = time_end - time_start;
            global_min = std::min(global_min, time_elapsed);
            print_datapoint(parameters, true, time_elapsed);
            store_parameter(time_elapsed);
            ah_report(htask, &time_elapsed);
            flag = !ah_converged(htask);
        }
        free_data();
        MPI_Bcast(&flag, 1, MPI_INT, 0, MPI_COMM_WORLD);
    }
    if (rank == 0) {
        printf("==Finished==\n");
        printf("==Best==\n");
        ah_best(htask);
        print_datapoint(parameters, true, global_min);
        ah_leave(htask);
        ah_free(hdesc);
    }
    MPI_Bcast(&parameters, 1, para_struct, 0, MPI_COMM_WORLD);
    //Run it 7 times
    std::vector<double> time_recorded;
    init_data();
    for (i = 0; i < 7; i++) {
        copy_data();
        MPI_Barrier(MPI_COMM_WORLD);
        if (rank == 0) {
            time_start = get_tick();
        }
        MPI_Barrier(MPI_COMM_WORLD);
        do_computation();
        MPI_Barrier(MPI_COMM_WORLD);
        if (rank == 0) {
            time_end = get_tick();
            time_recorded.push_back(time_end - time_start);
        }
    }
    std::sort(time_recorded.begin(), time_recorded.end());
    if (rank == 0) {
        printf("Elapsed Time (7 runs sorted increasingly):\n");
        for (i = 0; i < 7; i++) {
            printf("%.5lfs\n", time_recorded[i]);
        }
    }
    MPI_Type_free(&para_struct);
    MPI_Finalize();
    return 0;
}
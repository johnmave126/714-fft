#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex>
#include <vector>
#include <algorithm>
#include <mpi.h>
#include <fftw3.h>
#include <fftw3-mpi.h>

#include "fft.h"
std::complex<double> *data, *result1, *result2;
ptrdiff_t alloc_local, local_n0, local_0_start;
ptrdiff_t local_n1, local_1_start;

void init_data() {
    int i, j, k;
    data_f func;
    switch (data_type) {
    case DATA_REAL:
        func = &sine_step_data;
        break;
    case DATA_ZERO:
    default:
        func = &all_zero_data;
        break;
    }
    alloc_local = fftw_mpi_local_size_3d_transposed(dimx, dimy, dimz, MPI_COMM_WORLD, &local_n0, &local_0_start, &local_n1, &local_1_start);
    data = reinterpret_cast<std::complex<double>*>(fftw_alloc_complex(alloc_local));
    result1 = reinterpret_cast<std::complex<double>*>(fftw_alloc_complex(alloc_local));
    result2 = reinterpret_cast<std::complex<double>*>(fftw_alloc_complex(alloc_local));
    for (i = 0; i < local_n0; i++) {
        for (j = 0; j < dimy; j++) {
            for (k = 0; k < dimz; k++) {
                data[i * dimy * dimz + j * dimz + k] = func(local_0_start + i, j, k);
            }
        }
    }
}

void copy_data() {
    memcpy(result1, data, alloc_local * sizeof(std::complex<double>));
}

void do_computation() {
    fftw_plan plan;
    plan = fftw_mpi_plan_dft_3d(dimx, dimy, dimz, reinterpret_cast<double(*)[2]>(result1), reinterpret_cast<double(*)[2]>(result2), MPI_COMM_WORLD, FFTW_FORWARD, FFTW_ESTIMATE | FFTW_MPI_TRANSPOSED_OUT);
    fftw_execute(plan);
    fftw_destroy_plan(plan);
}


int main(int argc, char **argv) {
    double time_start, time_end;
    std::vector<double> time_recorded;

    MPI_Comm world;
    int i, num = 7;
    int rank, size;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    process_opt(argc, argv, rank, MPI_COMM_WORLD, NULL, NULL, NULL);
    if (rank == 0) {
        printf("==FFT FFTW Benchmark==\n");
        printf("==num_proc=%d dimx=%d dimy=%d dimz=%d==\n", size, dimx, dimy, dimz);
    }

    if (is_result_output) {
        num = 1;
    }
    init_data();

    for (i = 0; i < num; i++) {
        copy_data();
        MPI_Barrier(MPI_COMM_WORLD);
        if (rank == 0) {
            time_start = get_tick();
        }
        MPI_Barrier(MPI_COMM_WORLD);
        do_computation();
        MPI_Barrier(MPI_COMM_WORLD);
        if (rank == 0) {
            time_end = get_tick();
            time_recorded.push_back(time_end - time_start);
        }
    }

    if (is_result_output) {
        writeFFT(MPI_COMM_WORLD, output_filename, result2, dimy, dimx, dimz, local_n1, dimx, dimz, local_1_start, 0);
    }
    std::sort(time_recorded.begin(), time_recorded.end());
    if (rank == 0) {
        printf("Elapsed Time (7 runs sorted increasingly):\n");
        for (i = 0; i < num; i++) {
            printf("%.5lfs\n", time_recorded[i]);
        }
    }
    MPI_Finalize();
    fftw_free(data);
    fftw_free(result1);
    fftw_free(result2);
    return 0;
}

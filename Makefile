CXX=mpic++
CP=cp -rf
USER=$(shell id -un)
LOCAL_LDIR=$(HOME)/local/lib
LOCAL_IDIR=$(HOME)/local/include

LIBS=-lfftw3_mpi -lfftw3 -lm -lharmony
CXXFLAGS=-O2 -I$(LOCAL_IDIR) -L$(LOCAL_LDIR) $(LIBS)
DEPS=fft.h

.PHONY: all install clean

all: fft_fftw fft_overlay ah_test


install: all ah_test.def
	$(CP) fft_fftw /lustre/$(USER)/
	$(CP) fft_overlay /lustre/$(USER)/
	$(CP) ah_test /lustre/$(USER)/
	$(CP) ah_test.def /lustre/$(USER)/
	$(CP) fft_overlay.def /lustre/$(USER)/

fft_fftw: fft_fftw.o data.o util.o 
	$(CXX) -o $@ $^ $(CXXFLAGS)

fft_overlay: fft_overlay.o data.o util.o
	$(CXX) -o $@ $^ $(CXXFLAGS)

ah_test: ah_test.o util.o
	$(CXX) -o $@ $^ $(CXXFLAGS)

ah_test.o: ah_test.cpp
	$(CXX) -c -o $@ $< $(CXXFLAGS)

%.o: %.cpp $(DEPS)
	$(CXX) -c -o $@ $< $(CXXFLAGS)

clean:
	rm -rf *.o fft_fftw fft_overlay ah_test

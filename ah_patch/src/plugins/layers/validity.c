/**
 * validity.c (validity.so)
 *
 * Check non-affine validity of a configuration for FFT project
 */

#include "hlayer.h"
#include "session-core.h"
#include "hspace.h"
#include "hpoint.h"
#include "hutil.h"
#include "hsockutil.h"
#include "hcfg.h"

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <signal.h>

/*
 * Name used to identify this plugin layer.
 * All Harmony plugin layers must define this variable.
 */
const char hplugin_name[] = "validity";

/*
 * Configuration variables used in this plugin.
 * These will automatically be registered by session-core upon load.
 */
const hcfg_info_t hplugin_keyinfo[] = {
    { CFGKEY_VAL_DIMX, "100",
      "Dimension of x-axis" },
    { CFGKEY_VAL_DIMY, "100",
      "Dimension of y-axis" },
    { CFGKEY_VAL_DIMZ, "100",
      "Dimension of z-axis" },
    { CFGKEY_VAL_NPROC, "32",
      "Number of processors" },
    { NULL }
};

#define MAX_CMD_LEN  4096
#define MAX_TEXT_LEN 1024

/*
 * Structure to hold all data needed by an individual search instance.
 *
 * To support multiple parallel search instances, no global variables
 * should be defined or used in this plug-in layer.  They should
 * instead be defined as a part of this structure.
 */
struct hplugin_data {
    hspace_t local_space;
    int dimx, dimy, dimz, nproc;
};

/*
 * Internal helper function prototypes.
 */
static int   strategy_cfg(hplugin_data_t* data, hspace_t* space);
static int   check_validity(hplugin_data_t* data, hpoint_t* point);

/*
 * Allocate memory for a new search task.
 */
hplugin_data_t* validity_alloc(void)
{
    hplugin_data_t* retval = calloc(1, sizeof(*retval));
    if (!retval)
        return NULL;

    return retval;
}

/*
 * Initialize (or re-initialize) data for this search task.
 */
int validity_init(hplugin_data_t* data, hspace_t* space)
{
    // Make a copy of the search space.
    hspace_copy(&data->local_space, space);

    // Initialize layer variables.
    if (strategy_cfg(data, space) != 0)
        return -1;

    return 0;
}

int validity_generate(hplugin_data_t* data, hflow_t* flow, hpoint_t* point)
{
    flow->status = HFLOW_ACCEPT;
    if (!check_validity(data, point)) {
        flow->status = HFLOW_REJECT;
        flow->point.id = 0;
    }
    return 0;
}

/*
 * Free memory associated with this search task.
 */
int validity_fini(hplugin_data_t* data)
{
    hspace_fini(&data->local_space);
    free(data);
    return 0;
}

/*
 * Internal helper function implementation.
 */

int strategy_cfg(hplugin_data_t* data, hspace_t* space)
{
    data->dimx = hcfg_int(search_cfg, CFGKEY_VAL_DIMX);
    data->dimy = hcfg_int(search_cfg, CFGKEY_VAL_DIMY);
    data->dimz = hcfg_int(search_cfg, CFGKEY_VAL_DIMZ);
    data->nproc = hcfg_int(search_cfg, CFGKEY_VAL_NPROC);

    return 0;
}

#define LOCALIZE() long D, T1, W1, Px1, Py1, Fz, \
                    Fp1, Ux1, Uz1, Fu1, Fy1, Ry, \
                    T2, W2, Pz2, Px2, Fy2, Fp2, \
                    Uz2, Uy2, Fu2, Fx, V, S
#define CHECK_ASSIGN(var)   if(strcmp(data->local_space.dim[i].name, #var) == 0) { \
                                var = point->term[i].value.i; \
                                continue; \
                            }
#define CHECK_ASSIGN2(var)   if(strcmp(data->local_space.dim[i].name, #var) == 0) { \
                                var = 1<<point->term[i].value.i; \
                                continue; \
                            }

int check_validity(hplugin_data_t* data, hpoint_t* point)
{
    int i;
    LOCALIZE();
    int total = data->nproc, dimx = data->dimx, 
                dimy = data->dimy, dimz = data->dimz;
    long Dy;
    for (i = 0; i < point->len; ++i) {
        CHECK_ASSIGN2(D);
        CHECK_ASSIGN2(T1);
        CHECK_ASSIGN2(Py1);
        CHECK_ASSIGN2(Uz1);
        CHECK_ASSIGN2(W1);
        CHECK_ASSIGN2(T2);
        CHECK_ASSIGN2(Px2);
        CHECK_ASSIGN2(Uy2);
        CHECK_ASSIGN2(W2);
    }
    if(total % D != 0)
        return 0;
    Dy = total / D;
    if(T1 > (dimx + D - 1) / D)
        return 0;
    if(Py1 > (dimy + Dy - 1) / Dy)
        return 0;
    if(Uz1 > (dimz + Dy - 1) / Dy)
        return 0;
    if(W1 > ((dimx + D - 1) / D + T1 - 1) / T1)
        return 0;
    if(T2 > (dimz + Dy - 1) / Dy)
        return 0;
    if(Px2 > (dimx + D - 1) / D)
        return 0;
    if(Uy2 > (dimy + D - 1) / D)
        return 0;
    if(W2 > ((dimz + Dy - 1) / Dy + T2 - 1) / T2)
        return 0;
    return 1;
}

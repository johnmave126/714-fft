#!/bin/tcsh
#SBATCH -t 10:00
#SBATCH -N 2
#SBATCH --ntasks 32
#SBATCH -J "FFT with FFTW benchmark"
#SBATCH --mem=65536
#SBATCH --mail-user=shuhao@cs.umd.edu
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL

module unload intel
module load openmpi/1.10.2
cd /lustre/cmsc714-1bvc/
mpirun ./fft_fftw -x 1000 -y 1000 -z 1000 
mpirun ./fft_fftw -r -x 1000 -y 1000 -z 1000 

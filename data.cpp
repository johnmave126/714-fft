#include <math.h>
#include <complex>
#include "fft.h"

std::complex<double> all_zero_data(int x, int y, int z) {
    return std::complex<double>(0, 0);
}

//Some made up function for correctness check
std::complex<double> sine_step_data(int x, int y, int z) {
    std::complex<double> s1(sin(x * M_PI / 31) * sin(y * M_PI / 3 - M_PI/8) * sin(z * M_PI / 11 - 7 * M_PI / 16), cos(x * M_PI / 13) * cos(y * M_PI / 115 - M_PI/8) * cos(z * M_PI / 9 - 7 * M_PI / 16));
    std::complex<double> s2(sin(x * M_PI / 21 - 3 * M_PI / 4) * sin(y * 3 - M_PI / 17) * sin(z), cos(x - 3 * M_PI / 4) * cos(y - M_PI / 17) * cos(z * M_PI / 7));
    std::complex<double> st1( ((x > 30)?1:0)*((y > 10)?1:0)*((z > 40)?1:0), 0);
    std::complex<double> st2( ((x > 6)?1:0)*((y > 12)?1:0)*((z > 3)?1:0), ((y > 18)?1:0)*((z > 9)?1:0));
    return s1 + s2 + st1 + st2;
}

#!/bin/tcsh
#SBATCH -t 10:00
#SBATCH -p debug
#SBATCH -N 1
#SBATCH -J "Active Harmony Test"
#SBATCH --mem-per-cpu=1024
#SBATCH --mail-user=shuhao@cs.umd.edu
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL

module unload intel
module load openmpi
cd /lustre/cmsc714-1bvc/
setenv HARMONY_HOME /lustre/cmsc714-1bvc/
mpirun -n 20 ./ah_test LOG_FILE=ah_local.log

#!/bin/tcsh
set dirs=`find . ! -path . -type d | grep -oP ".*"`
foreach dir ($dirs)
    cd $dir
    sbatch run.sh
    cd ..
end

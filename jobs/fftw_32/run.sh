#!/bin/tcsh
#SBATCH -t 5:00
#SBATCH -N 2
#SBATCH --ntasks 32
#SBATCH --share
#SBATCH -p debug
#SBATCH -J "FFT with FFTW benchmark"
#SBATCH --mem=100000
#SBATCH --mail-user=shuhao@cs.umd.edu
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL

module unload intel
module load openmpi/1.10.2
cd /lustre/cmsc714-1bvc/
mpirun --mca mpi_cuda_support 0 --mca mpi_warn_on_fork 0 ./fft_fftw -x 1000 -y 1000 -z 1000

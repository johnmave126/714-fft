#ifndef _FFT_H
#define _FFT_H

#include <complex>
#include <fftw3.h>
#include <mpi.h>

#define DATA_ZERO (0)
#define DATA_REAL (1)

extern int data_type;
extern bool is_result_output;
extern const char* output_filename;
extern int dimx, dimy, dimz;

typedef void(*help_printer)();
typedef void(*opt_handler)(int, const char*);

/**
 * Process the command line arguments
 */
void process_opt(int argc, char** argv, int rank, MPI_Comm comm, const char* addon, opt_handler hopt, help_printer hprt);

double get_tick();

/**
 * File helper
 */
void writeFFT(MPI_Comm comm, const char* filename, std::complex<double>* data, int ax, int ay, int az, int nx, int ny, int nz, int off_x, int off_y);


/**
 * Data generating functions
 */
typedef std::complex<double>(*data_f)(int x, int y, int z);

std::complex<double> all_zero_data(int x, int y, int z);
std::complex<double> sine_step_data(int x, int y, int z);

#endif

#!/bin/tcsh
#SBATCH -t 2:00:00
#SBATCH -N 2
#SBATCH --ntasks=16
#SBATCH --share
#SBATCH -p debug
#SBATCH -J "FFT with NEW benchmark Test"
#SBATCH --mem=100000
#SBATCH --mail-user=shuhao@cs.umd.edu
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL
#variables: random/penalty seed task_num

module unload intel
module load openmpi/1.10.2
#set job=`sbatch --dependency=afternotok:$SLURM_JOBID run.sh | grep -oP"\d+$"`
cd /lustre/cmsc714-1bvc/
setenv PATH ${PATH}:/lustre/cmsc714-1bvc/local/bin
setenv HARMONY_HOME /lustre/cmsc714-1bvc/local
rm ah_log_overlay_test.bin
mpirun --mca mpi_cuda_support 0 --mca mpi_warn_on_fork 0 ./fft_overlay -p -s 42 -l "ah_log_overlay_test.bin" -x 1000 -y 1000 -z 1000
#scancel $job

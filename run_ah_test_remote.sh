#!/bin/tcsh
#SBATCH -t 10:00
#SBATCH -p debug
#SBATCH -N 1
#SBATCH -J "Active Harmony Test"
#SBATCH --mem-per-cpu=1024
#SBATCH --mail-user=shuhao@cs.umd.edu
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL

module unload intel
module load openmpi
cd /lustre/cmsc714-1bvc/
ssh -f -N -L 1979:ah.youmu.moe:1979 login.deepthought2.umd.edu >&/dev/null
setenv HARMONY_HOST ah.youmu.moe
setenv HARMONY_PORT 1979
setenv HARMONY_HOME /lustre/cmsc714-1bvc/local
mpirun -n 20 ./ah_test LOG_FILE=ah_remote.log
pgrep -u cmsc714-1bvc ssh | xargs kill -9
